document.addEventListener('deviceready', function () {
    // ici du code au lancement de l'application 
}, false)

var app = angular.module('app', ['ng'],
    ['$provide', function ($provide) {
        $provide.decorator('$exceptionHandler', ['$delegate', function ($delegate, $injector) {
            return function (exception, cause) {
                alert("Impossible de récupérer les information");
                $(".row.ng-scope").remove();
            };
        }])
    }]
);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/home', { templateUrl: 'partials/home.html' })
        .when('/about', { templateUrl: 'partials/about.html' })
        .otherwise({ redirectTo: '/home' });
})
