function HomeCtrl($scope, $http) {

    $scope.champRecherche1 = 60100
    $scope.loader = false;

    $scope.search = function () {

        var url = "https://odata-inpn.mnhn.fr/taxa?size=50&embed=OPERATIONAL_GROUPS,GENERAL_PUBLIC_GROUP,DESCRIPTION,PHOTOS&occurrenceLocationId=INSEEC" + $scope.champRecherche1 + "";

        $scope.loader = true;

        // disableHomeForm();
        $http.get(url)
            .success(httpSuccess)
            .error(httpError);
        // enableHomeForm();

        function httpError(params) {
            $scope.loader = false;
            alert("Impossible de récupérer les information");
            enableHomeForm();
        }

        function httpSuccess(response) {

            $scope.loader = false;
            $scope.responseWS1 = response._embedded.taxa;

            
        }

        function enableHomeForm() {
            $(".home").removeClass("disabled");
            $(".homeloader").removeClass("loader");
            $("input").prop('disabled', false);
        }

    }


}


// https://odata-inpn.mnhn.fr/taxa?embed=OPERATIONAL_GROUPS,GENERAL_PUBLIC_GROUP,DESCRIPTION,PHOTOS&occurrenceLocationId=INSEEC77186

// https://api.openweathermap.org/geo/1.0/direct?q=London&limit=5&appid=bb23b32eb38ceb2e8b05bcc988fb1587

// {
//     "name": "London",
//     "lat": 51.5073219,
//     "lon": -0.1276474,
//     "country": "GB",
//     "state": "England"
// }

